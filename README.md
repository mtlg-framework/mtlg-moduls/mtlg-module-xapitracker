# xAPI-Tracker Module

This module uses the eventtracker module to track interactions of the players and sends xAPI statements via the xapidatacollector module to the LRS.

The module has to be started with the `start` function in order to track events. As first parameter you can set if high level events should be 
tracked and with the second boolean parameter if low level events should be tracked.
```javascript
MTLG.xapitracker.start(true, false); // only high level events are tracked
```

For manually determining which actor did an interaction you can set a callback for low level as well as for high level events like
following:
```javascript
// set callback for setting an actor for high level events
MTLG.xapitracker.setCheckActorFunctionHighLevel((interaction) => {
    ...
    return actor; // return actor based on interaction
  })
```
And the same for low level events with `setCheckActorFunctionLowLevel`.

If you want to customize the defined xapi statements you can register a callback that gets called before the statement 
is sent to the lrs.
```javascript
MTLG.xapitracker.registerXapiStatementCallback((statement) => {
    // change statement
    return statement;
})
```
You can also return `null`, then the statement will not be sent to the lrs via the xapidatacollector module. 

