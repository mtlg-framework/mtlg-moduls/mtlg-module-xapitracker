

function firstlevel_init(){


  // calling a function of the new module
  MTLG.xapitracker.setCheckActorFunctionHighLevel((interaction) => {
    console.log("Interaction from xapitracker", interaction)
    return {
      objectType: "Agent",
      id: "mailto:dummy2@player.com",
      name: "DummyPlayer2"
    };
  })
  MTLG.xapitracker.start(true, false);

  drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel === "level1"){
    return 1;
  }
  return 0;
}


function drawLevel1(){
  var stage = MTLG.getStageContainer();

  var circle1 = new createjs.Shape();
  circle1.graphics.beginFill("red").drawRect(-50, -50, 100, 100);
  circle1.x = 300;
  circle1.y = 500;
  circle1.setBounds(-50, -50, 100, 100)
  circle1.name = "red"
  stage.addChild(circle1)

  circle1.on("pressmove", function(evt) {
    console.log("pressmove", evt);
    evt.target.set(stage.globalToLocal(evt.stageX, evt.stageY));
  });

  let test = MTLG.utils.uiElements.addButton({text: "Test High Level", sizeX: 300, sizeY: 70}, () =>{
    console.log('button')
  }).set({x:.5*MTLG.getOptions().width, y:.3*MTLG.getOptions().height});
  stage.addChild(test);

}
