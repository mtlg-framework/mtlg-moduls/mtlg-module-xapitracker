let checkActorHighLevel;
let checkActorLowLevel;

let _trackHigh;
let _trackLow;

/**
 * Dummy callback function for adjusting the xapi statement. This function only returns the given statement without
 * changing anything.
 * @param statement
 * @returns {*}
 */
let xapiStatementCallback = function(statement) {
    return statement;
};

let init = function( options, callback ){
    checkActorHighLevel = checkActorDummyFunction;
    checkActorLowLevel = checkActorDummyFunction;
};

/**
 * Function to start the xapi tracker. Both levels can be tracked at the same time.
 * @param trackHigh if true, high level events are tracked
 * @param trackLow if true, low level events are tracked
 */
let start = function(trackHigh = true, trackLow = true) {
    if(!MTLG.eventtracker) {
        console.error("The eventtracker module must be included otherwise this module does not work.")
    } else {
        _trackHigh = trackHigh;
        _trackLow = trackLow;

        MTLG.eventtracker.addCallback(sendEvent);
    }
}

/**
 * This function registers a callback. The callback gets called with the predefined xapi statement. In the callback this
 * statement can be changed and returned so that the returned statement will be sent. If null is return, nothing happens.
 * @param callback function for adjusting the xapi statement
 */
let registerXapiStatementCallback = function(callback) {
    xapiStatementCallback = callback;
}

/**
 * Helper function for determine the verb and context of the interaction
 * @param interaction
 * @returns {{verb: {display: {"de-DE": string, "en-US": string}, id: string}, context: {extensions: {"http://example.com/endPosition": {x: *, y: *}}}}|{verb: {display: {"de-DE": string, "en-US": string}, id: string}, context: {extensions: {"http://example.com/endPosition": {x: *, y: *}, "http://example.com/startPosition": {x: *, y: *}}}}}
 */
let getVerbContext = function(interaction) {
    if(interaction.path.length > 2) {
        return {
            verb: {
                id: "http://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/dragged",
                display: {
                    "en-US": "dragged",
                        "de-DE": "zog"
                }
            },
            context: {
                extensions: {
                    "http://example.com/endPosition": {x: interaction.path[interaction.path.length - 1].x, y: interaction.path[interaction.path.length - 1].y},
                    "http://example.com/startPosition": {x: interaction.path[0].x, y: interaction.path[0].y}
                }
            }
        };
    } else {
        return {
            verb: {
                id: "http://xapi.elearn.rwth-aachen.de/definitions/multitouch/verbs/clicked",
                display: {
                    "en-US": "clicked",
                    "de-DE": "klickte"
                }
            },
            context: {
                extensions: {
                    "http://example.com/endPosition": {x: interaction.path[0].x, y: interaction.path[0].y}
                }
            }
        };
    }
}

/**
 * This function sends a xapi statement to an lrs via the mtlg-module-xapidatacollector.
 * It calls the actor function and if registered also the a callback for changing the
 * xapi statement before sending.
 * @param interaction Interaction from mtlg-module-eventtracker
 * @param actor if the actor is given, the actor will not be determined anymore.
 * @returns {Promise<void>}
 */
async function sendEvent(interaction, actor = null) {
    if(actor == null) {
        if(_trackHigh && interaction.high_level && _trackLow) {
            sendEvent(interaction, await checkActorHighLevel(interaction));
            sendEvent(interaction, await checkActorLowLevel(interaction));
            return;
        } else if (_trackHigh && interaction.high_level){
            actor = await checkActorHighLevel(interaction);
        } else if(_trackLow) {
            actor = await checkActorLowLevel(interaction);
        } else {
            return;
        }
    }
    const {verb, context} = getVerbContext(interaction);
    let stmt = {
        actor: actor,
        verb: verb,
        object: {
            id : "http://sendActivityStatementFunction"
        },
        "context": context
    };
    stmt = await xapiStatementCallback(stmt);
    if(stmt === null) {
        return;
    }
    MTLG.xapidatacollector.sendStatement(stmt).then(function (stmtID) {
        console.log('then',stmtID);
    }).catch(function (error){
        console.log('catch', error.message, error);
    });
}

/**
 * This function is the dummy function for checking for actor. When no function is set, this function gets called
 * and returns a dummy agent
 */
let checkActorDummyFunction = function () {
    return MTLG.getOptions().xapi.actor || {
        objectType: "Agent",
        id: "mailto:dummy@player.com",
        name: "DummyPlayer"
    };
}

/**
 * This function sets the function that get called, to determine which actor did the low level interaction.
 * @param func Function with one parameter. The call receives the interaction as parameter.
 */
let setCheckActorFunctionLowLevel = function (func) {
    checkActorLowLevel = func;
}

/**
 * This function sets the function that get called, to determine which actor did the high level interaction.
 * @param func Function with one parameter. The call receives the interaction as parameter.
 */
let setCheckActorFunctionHighLevel = function (func) {
    checkActorHighLevel = func;
}

let info = function() {
    return {
        version: '1',
        name: 'xapitracker',
        type: 'No type added.',
        note: 'This module tracks events to send xapi statements.'
    }
};

// injecting into MTLG and expoxe public methods
MTLG.xapitracker = {
    start,
    setCheckActorFunctionLowLevel,
    setCheckActorFunctionHighLevel,
    registerXapiStatementCallback
};
MTLG.addModule(init, info);
